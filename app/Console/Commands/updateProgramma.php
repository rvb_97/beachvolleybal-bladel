<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use App\Programma as Programma;
use DB;
use Storage;
use Slack;

class updateProgramma extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:programma';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates the program via a cronjob';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $day = date('l');
        if( strtolower($day) == 'saturday' ) {
            $day = 'zaterdag';
        } elseif( strtolower($day) == 'sunday') {
            $day = 'zondag';
        }

        //$oldProgramma = NULL;
        // Make backup of the other day.
        /*   
        if($day == 'zondag') {
            $programma = DB::table('programma')->get();
            foreach ($programma as $wedstrijd) {
                if($wedstrijd->dag == 'Zaterdag') {
                    $data = new \stdClass();
                    $data->ronde = $wedstrijd->ronde;
                    $data->starttijd = $wedstrijd->starttijd;
                    $data->poule = $wedstrijd->poule;
                    $data->team1 = $wedstrijd->team1;
                    $data->team2 = $wedstrijd->team2;
                    $data->scheidsrechter = $wedstrijd->scheidsrechter;
                    $data->uitslagen = $wedstrijd->uitslagen;
                    $data->dag = $wedstrijd->dag;
                    $data->dagdeel = $wedstrijd->dagdeel;

                    $oldProgramma[] = $data;
                }
            }
        } 
        elseif($day == 'zaterdag') {
            $programma = Programma::all();
            foreach ($programma as $wedstrijd) {
                if($wedstrijd->dag == 'Zondag') {
                    $data = new \stdClass();
                    $data->ronde = $wedstrijd->ronde;
                    $data->starttijd = $wedstrijd->starttijd;
                    $data->poule = $wedstrijd->poule;
                    $data->team1 = $wedstrijd->team1;
                    $data->team2 = $wedstrijd->team2;
                    $data->scheidsrechter = $wedstrijd->scheidsrechter;
                    $data->uitslagen = $wedstrijd->uitslagen;
                    $data->dag = $wedstrijd->dag;
                    $data->dagdeel = $wedstrijd->dagdeel;

                    $oldProgramma[] = $data;
                }
            }
        }*/

        // Check if file exists
        $exists = Storage::disk('local')->has('Uitslagen_Site.csv');
        if($exists) {

            // First, delete existing rows preventing duplicate entries in the database.
            //Programma::where('dag', 'zaterdag')->delete();

            // Get data from local storage that has been uploaded via FTP
            $data = Storage::disk('local')->get('Uitslagen_Site.csv');

            $csvFile = explode("\r\n", $data);
            $data = [];
            $i = 0;
            foreach ($csvFile as $line) 
            {
                // Skip first row
                if($i != 0) {
                    if($line == '0') {
                      continue;
                    }

                    array_push($data, explode(';', $line));
                }
                
                $i++;
            }

            foreach ($data as $value) 
            {
                // Check for empty row
                if($value[3] != '0' && $value[4] != '0') {
                    $dagdeel = strtolower($value[8]);

                    Programma::updateOrCreate(
                        [
                            'team1' => (isset($value[3]) ? $value[3] : ''),
                            'team2' => (isset($value[4]) ? $value[4] : ''),
                            'scheidsrechter' => (isset($value[5]) ? $value[5] : '')
                        ],
                        [
                            'ronde' => (isset($value[0]) ? str_replace('""', '', $value[0]) : ''),
                            'starttijd' => (isset($value[1]) ? str_replace('""', '', $value[1]) : ''),
                            'poule' => (isset($value[2]) ? str_replace('""', '', $value[2]) : ''),
                            'team1' => (isset($value[3]) ? str_replace('""', '', $value[3]) : ''),
                            'team2' => (isset($value[4]) ? str_replace('""', '', $value[4]) : ''),
                            'scheidsrechter' => (isset($value[5]) ? str_replace('""', '', $value[5]) : ''),
                            'uitslagen' => (isset($value[6]) ? str_replace('""', '', $value[6]) : ''),
                            'dag' => (isset($value[7]) ? str_replace('""', '', $value[7]) : ''),
                            'dagdeel' => (isset($dagdeel) ? str_replace('""', '', ucfirst($dagdeel)) : '')
                        ]
                    );
                }

                /*
                DB::table('programma')->insert([
                    'ronde' => (isset($value[0]) ? $value[0] : ''),
                    'starttijd' => (isset($value[1]) ? $value[1] : ''),
                    'poule' => (isset($value[2]) ? $value[2] : ''),
                    'team1' => (isset($value[3]) ? $value[3] : ''),
                    'team2' => (isset($value[4]) ? $value[4] : ''),
                    'scheidsrechter' => (isset($value[5]) ? $value[5] : ''),
                    'uitslagen' => (isset($value[6]) ? $value[6] : ''),
                    'dag' => (isset($value[7]) ? $value[7] : ''),
                    'dagdeel' => (isset($value[8]) ? $value[8] : ''),
                ]);*/
            }

            Storage::delete('Uitslagen_Site.csv');

            // Insert backup - Enable this when we are on sunday
            /*if($oldProgramma != NULL) { 
                foreach ($oldProgramma as $wedstrijd) {
                    $result = DB::table('programma')->insert([
                        'ronde' => (isset($wedstrijd->ronde) ? $$wedstrijd->ronde : ''),
                        'starttijd' => (isset($wedstrijd->starttijd) ? $wedstrijd->starttijd : ''),
                        'poule' => (isset($wedstrijd->poule) ? $wedstrijd->poule : ''),
                        'team1' => (isset($wedstrijd->team1) ? $wedstrijd->team1 : ''),
                        'team2' => (isset($wedstrijd->team2) ? $wedstrijd->team2 : ''),
                        'scheidsrechter' => (isset($wedstrijd->scheidsrechter) ? $wedstrijd->scheidsrechter : ''),
                        'uitslagen' => (isset($wedstrijd->uitslagen) ? $wedstrijd->uitslagen : ''),
                        'dag' => (isset($wedstrijd->dag) ? $wedstrijd->dag : ''),
                        'dagdeel' => (isset($wedstrijd->dagdeel) ? $wedstrijd->dagdeel : ''),
                    ]);
                }
            }*/

            DB::table('cron_updates')->insert([
                'programma' => 'Succesvol',
                'ranking' => '',
                'timestamp' => DB::raw('NOW()')
            ]);

            /*
            DB::table('programma')
                ->orderBy('id_programma', 'desc')
                ->limit(1)
                ->delete();
            */

            Slack::send('Programma is succesvol geupdate!');
        }
    }
}
